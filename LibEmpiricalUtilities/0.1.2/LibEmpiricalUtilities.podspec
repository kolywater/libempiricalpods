Pod::Spec.new do |s|
s.name             = "LibEmpiricalUtilities"
s.version          = "0.1.2"
s.summary          = "A useful collection of iOS classes"

s.description      = "A toolbox of useful classes and abstractions to make coding faster and more future proof"

s.homepage         = "https://bitbucket.org/kolywater/libempiricalutilities"
s.license          = 'MIT'
s.author           = { "Greg Elliott" => "greg@empiric.al" }
s.source           = { :git => "https://bitbucket.org/kolywater/libempiricalutilities.git", :tag => s.version.to_s }

s.platform     = :ios, '8.0'
s.requires_arc = true

s.source_files = 'Pod/**/*'
end
